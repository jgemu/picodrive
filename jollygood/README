PicoDrive
---------
PicoDrive is an emulator for the Sega 32X and related systems.

The Jolly Good API port is designed to be used exclusively with 32X content.

Source for the emulator core is unmodified from upstream sources.

Compiling
---------
Make sure you have The Jolly Good API's header files installed. If you did
not install them, you will be required to include their path in CFLAGS.

Options:
  DISABLE_MODULE - Set to a non-zero value to disable building the module.
  ENABLE_STATIC_JG - Set to a non-zero value to build a static JG archive.

Linux:
  cd jollygood
  make

macOS:
  cd jollygood
  make

BSD:
  cd jollygood
  gmake

Windows (MSYS2):
  cd jollygood
  make

Cross Compile:
(For example compiling on Linux for MinGW)
  AR=x86_64-w64-mingw32-ar \
  CC=x86_64-w64-mingw32-cc \
  PKG_CONFIG=x86_64-w64-mingw32-pkg-config \
  STRIP=x86_64-w64-mingw32-strip \
  make

The build will be output to "picodrive/". This directory may be used as is
locally by copying it to your local "cores" directory, or may be installed
system-wide using the "install" target specified in the Makefile.

Input Devices
-------------
The Jolly Good API port of PicoDrive only supports the 6 Button Control Pad.
